<?php

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'MiniModule\Controller\Index',
                        'action' => 'index'
                    ),
                ),
            ),
            'action' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/:action',
                    'constraints' => array(),
                    'defaults' => array(
                        'controller' => 'MiniModule\Controller\Index',
                        'action' => 'index'
                    ),
                ),
            ),
            'inscription' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/inscription',
                    'defaults' => array(
                        'controller' => 'MiniModule\Controller\Index',
                        'action' => 'inscription',
                    ),
                ),
            ),
            'connection' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/connection',
                    'defaults' => array(
                        'controller' => 'MiniModule\Controller\Index',
                        'action' => 'connection',
                    ),
                ),
            ),
            'default' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/:action',
                    'constraints' => array(),
                    'defaults' => array(
                        'controller' => 'MiniModule\Controller\Index',
                        'action' => 'index'
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            '404' => __DIR__ . '/../view/404.phtml',
            'error' => __DIR__ . '/../view/error.phtml',
            'errorlog' => __DIR__ . '/../view/errorlog.phtml',
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'mini-module/index/index' => __DIR__ . '/../view/mini-module/index/index.phtml',
//            'mini-module/index/form' => __DIR__ . '/../view/mini-module/index/form.phtml',
            'layout/menuutilisateur' => __DIR__ . '/../view/layout/menuutilisateur.phtml',
            'layout/menuvisiteur' => __DIR__ . '/../view/layout/menuvisiteur.phtml',
        ),
        'template_path_stack' => array(__DIR__ . '/../view/',),
    ),
    'controllers' => array(
        'invokables' => array(
            'MiniModule\Controller\Index' => 'MiniModule\Controller\IndexController',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'DbAdapterFactory' => 'MiniModule\Services\Factory\DbAdapterFactory',
        ),
        'services' => array(
            'session' => new \Zend\Session\Container('minimodule'),
        ),
    ),
);
