<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MiniModule;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Router\Http\Literal;
use Zend\Mvc;
use MiniModule\Model\User;
use MiniModule\Model\UserTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use MiniModule\Form\ConnectionForm;

/**
 * Module
 *  configuration et initialisation du site
 *
 * @package     MiniModule
 * @category    classes
 */
class Module implements ConfigProviderInterface, BootstrapListenerInterface, AutoloaderProviderInterface {

    public function getConfig() {
        return include __DIR__ . "/config/module.config.php";
    }

    public function onBootstrap(EventInterface $e) {
        $application = $e->getTarget();
        $event = $application->getEventManager();
        $event->attach(MvcEvent::EVENT_DISPATCH_ERROR, function (MvcEvent $e) {
            error_log('DISPATCH_ERROR : ' . $e->getError());
            error_log($e->getControllerClass() . ' ' . $e->getController());
        });
        $event->attach(MvcEvent::EVENT_RENDER_ERROR, function (MvcEvent $e) {
            error_log('RENDER_ERROR : ' . $e->getError());
        });

        $event->attach(MvcEvent::EVENT_RENDER, function (MvcEvent $e) {
            $services = $e->getApplication()->getServiceManager();
//            $form = $services->get('MiniModule\Form\Authentification');
            $session = $services->get('session');
            $menu = new ViewModel();
            if (!isset($session->user)) {
//                $form = $services->get('MiniModule\Form\Authentification');
                $form = new ConnectionForm();


//                $formUser = $services->get('MiniModule\Form\NewUser');
//                $rightViewModel->setVariables(array('form' => $form, 'newUserForm' => $formUser));
                $menu->setTemplate('layout/menuvisiteur');
            } else {
                $menu->setVariables(array('user' => $session->user));
                $menu->setTemplate('layout/menuutilisateur');
            }
            $view = $e->getViewModel(); // c'est le viewModel qui contient le layout (top viewModel)
//            $formViewManager = new ViewModel(array('form' => $form));
//            $formViewManager->setTemplate('layout/form-auth');
//            $view->addChild($formViewManager, 'formulaireAuth');
            $view->addChild($menu, 'menu');
        });
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'MiniModule\Model\UserTable' => function($sm) {
                    $tableGateway = $sm->get('UserTableGateway');
                    $table = new UserTable($tableGateway);
                    return $table;
                },
                'UserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }

}
