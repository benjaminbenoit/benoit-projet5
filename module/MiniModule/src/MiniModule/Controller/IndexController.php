<?php

namespace MiniModule\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Factory;
use Zend\Form\Form;
use Zend\View\Model\ViewModel;
use MiniModule\Form\InscriptionForm;
use MiniModule\Form\ConnectionForm;
use MiniModule\Model\User;

/**
 * IndexController
 * Partie générale du site, comprenant la gestion des utilisateurs
 *
 * @package     MiniModule\Controller
 * @category    classes
 */
class IndexController extends AbstractActionController {

    /**
     * table User
     * @var \MiniModule\Model\UserTable $userTable
     */
    protected $userTable;

    /**
     * Page d'accueil
     */
    public function indexAction() {
        return array();
    }

    /**
     *Formulaire de connection utilisateur
     */
    public function connectionAction() {
        $form = new ConnectionForm();

        return array('form' => $form);
    }

    /**
     * Permet la connection
     */
    public function formAction() {
        $services = $this->getServiceLocator();
        $form = new ConnectionForm();
//        $form->get('submit')->setValue('S\'inscrire');
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $adapter = new \Zend\Db\Adapter\Adapter($this->getServiceLocator()->get('config')['db']);
                $adapter = $services->get('DbAdapterFactory');
                $sql = "Select * from user where pseudo = ? and mdp = ?";
                $sql_result = $adapter->createStatement($sql, array($form->get('pseudo')->getValue(), $form->get('mdp')->getValue()))->execute();
                if ($sql_result->count() > 0) {
                    $services->get('session')->user = $form->get('pseudo')->getValue();
                    $vm = new ViewModel();
                    $vm->setVariables($form->getData());
                    $vm->setTemplate('mini-module/index/index');
                    return $vm;
                } else {
                    $vm = new ViewModel();
                    $vm->setTemplate('errorlog');
                    return $vm;
                }
            }
        }
        return array();
    }

    /**
     * Formulaire d'inscription utilisateur
     */
    public function inscriptionAction() {
        $form = new InscriptionForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $user = new User();
            $form->setInputFilter($user->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $user->exchangeArray($form->getData());
                $this->getUserTable()->saveUser($user);

                return $this->redirect()->toRoute('home');
            }
        }
        return array('form' => $form);
    }

    /**
     * Réuperer les utilisateurs (fonction de test)
     */
    public function getUserTable() {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('MiniModule\Model\UserTable');
        }
        return $this->userTable;
    }

    /**
     * Déconnection
     */
    public function deconnectAction() {
        $services = $this->getServiceLocator();
        unset($services->get('session')->user);
        return $this->redirect()->toRoute('home');
    }

}
