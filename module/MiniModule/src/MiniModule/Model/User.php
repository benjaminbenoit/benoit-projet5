<?php

namespace MiniModule\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * User
 * Classe PHP qui représente l'objet User
 *
 * @package     MiniModule\Model
 * @category    classes
 */
class User {

    /**
     * Id utilisateur
     * @var int $id
     */
    public $id;
    /**
     * Pseudo utilisateur
     * @var string $pseudo
     */
    public $pseudo;
    /**
     * Mot de passe utilisateur
     * @var string $mdp
     */
    public $mdp;
    /**
     * Code formule d'un utilisateur
     * @var string $codeformule
     */
    public $codeformule;
    /**
     * Filtres de validation
     * @var array $inputFilter
     */
    protected $inputFilter;

    /**
     * Copies le données passés vers l'entité
     * @param $data
     */
    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->pseudo = (!empty($data['pseudo'])) ? $data['pseudo'] : null;
        $this->mdp = (!empty($data['mdp'])) ? $data['mdp'] : null;
        $this->codeformule = (!empty($data['codeformule'])) ? $data['codeformule'] : null;
    }

    /**
     * Exception setInputFilter
     * @param $inputFilter
     */
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Filtres
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

//            $inputFilter->add(array(
//                'name' => 'id',
//                'required' => true,
//                'filters' => array(
//                    array('name' => 'Int'),
//                ),
//            ));

            $inputFilter->add(array(
                'name' => 'pseudo',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'mdp',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ));

            $inputFilter->add(array(
                'name' => 'codeformule',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}

?>