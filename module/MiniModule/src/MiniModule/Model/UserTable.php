<?php

namespace MiniModule\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * UserTable
 * Données de la table Users
 *
 * @package     MiniModule\Model
 * @category    classes
 */
class UserTable {

    /**
     * Variable pour l'instance passée dans le constructeur
     * @var string $tableGateway
     */
    protected $tableGateway;

    /**
     * Constructeur
     */
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Retourne les utilisateurs
     * @return ResultSet $resultSet
     */
    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Retourne un utilisateur avec l'id en paramètre
     * @param $id
     * @return row $row
     */
    public function getUser($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Utilisateur inexistant $id");
        }
        return $row;
    }

    /**
     * Enregistrer un utilisateur
     * @param $user
     */
    public function saveUser(User $user) {
        $data = array(
            'pseudo' => $user->pseudo,
            'mdp' => $user->mdp,
            'codeformule' => $user->codeformule,
        );

        $id = (int) $user->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUser($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('id d\'utilisateur inexistant');
            }
        }
    }

    /**
     * Supprimer un utilisateur
     * @param $id
     */
    public function deleteUser($id) {
        $this->tableGateway->delete(array('id' => (int) $id));
    }

}
