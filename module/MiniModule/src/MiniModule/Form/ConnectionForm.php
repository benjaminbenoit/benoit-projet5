<?php

namespace MiniModule\Form;

use Zend\Form\Form;

/**
 * ConnectionForm
 * Création du formulaire de connection
 *
 * @package     MiniModule\Form
 * @category    classes
 */
class ConnectionForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('user');

//        $this->add(array(
//            'name' => 'id',
//            'type' => 'Hidden',
//        ));
        $this->add(array(
            'name' => 'pseudo',
            'type' => 'Text',
            'options' => array(
                'label' => 'Pseudo : ',
            ),
            'attributes' => array(
                'placeholder' => 'Pseudo',
            ),
        ));
        $this->add(array(
            'name' => 'mdp',
            'type' => 'Password',
            'options' => array(
                'label' => 'Mot de passe : ',
            ),
            'attributes' => array(
                'placeholder' => 'Mot de passe',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Se connecter',
                'class' => 'btn btn-primary',
                'id' => 'submitbutton',
            ),
        ));
    }

}
