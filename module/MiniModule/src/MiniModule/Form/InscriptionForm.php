<?php

namespace MiniModule\Form;

use Zend\Form\Form;
use Zend\Form\Element\Captcha;
use Zend\Captcha\Image as CaptchaImage;

/**
 * InscriptionForm
 * Création du formulaire d'inscription
 *
 * @package     MiniModule\Form
 * @category    classes
 */
class InscriptionForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('user');

//        $this->add(array(
//            'name' => 'id',
//            'type' => 'Hidden',
//        ));
        $this->add(array(
            'name' => 'pseudo',
            'type' => 'Text',
            'options' => array(
                'label' => 'Pseudo : ',
            ),
            'attributes' => array(
                'placeholder' => 'Pseudo',
            ),
        ));
        $this->add(array(
            'name' => 'mdp',
            'type' => 'Password',
            'options' => array(
                'label' => 'Mot de passe : ',
            ),
            'attributes' => array(
                'placeholder' => 'Mot de passe',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'S\'inscrire',
                'class' => 'btn btn-primary',
                'id' => 'submitbutton',
            ),
        ));
        $this->add(array(
            'name' => 'codeformule',
            'type' => 'Radio',
            'options' => [
                'value_options' => [
                    '0' => '1 Mo',
                    '1' => '10 Mo',
                    '2' => '100 Mo',
                ]
            ],
            'attributes' => [
                'value' => '0' // This set the opt 1 as selected when form is rendered
            ]
        ));
        $this->add(array(
            'type' => '\Zend\Form\Element\Captcha',
            'name' => 'captcha',
            'options' => array(
                'captcha' => array(
                    'class' => 'Dumb',
                ),
            ),
        ));
    }

}
